# Use a PHP base image
FROM php:8.0-apache

# Set the working directory
WORKDIR /My_website/Djangi-project

# Copy the PHP website files to the container
COPY . /My_website

# Install PHP dependencies (if any)
# RUN composer install

# Expose port 80 for Apache
EXPOSE 3040

# Start the Apache server
CMD ["apache2-foreground"]
